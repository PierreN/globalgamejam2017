﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowScript : MonoBehaviour {

    public Transform playertransform;
	
	// Update is called once per frame
	void Update () {

        transform.position = playertransform.position;
	}
}
