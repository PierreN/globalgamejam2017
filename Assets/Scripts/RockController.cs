﻿using UnityEngine;

public class RockController : MovableObject
{
    private Rigidbody rb;
    private bool canMove;
    private float timer;

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        timer = 0;
        canMove = true;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (timer > 0.5f)
        {
            canMove = true;
            timer = 0;
        }
        if (rb.velocity.magnitude == 0)
            rb.isKinematic = true;
    }

    public override void OnSoundWaveHit(Vector3 soundWaveDirection)
    {
        if (canMove)
        {
            canMove = false;
            rb.isKinematic = false;
            rb.AddForce(soundWaveDirection * 20 * rb.mass, ForceMode.Impulse);
        }
    }

}
