﻿using UnityEngine;

public abstract class MovableObject : MonoBehaviour
{
    public abstract void OnSoundWaveHit(Vector3 soundWaveDirection);
}
