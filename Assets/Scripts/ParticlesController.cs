﻿using System.Collections;
using UnityEngine;
using System.Linq;

public class ParticlesController : MonoBehaviour
{

    public float deathTimer;

    public LayerMask ObstacleLayer;
    public LayerMask ParticlesLayer;
    public int playerOrigin;

    private Rigidbody rb;
    private SphereCollider col;

    // Use this for initialization
    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        col = GetComponent<SphereCollider>();
    }

    void Start()
    {
        StartCoroutine("DeathTimer");
        StartCoroutine("ActivateCollider");
    }

    void Update()
    {
        Vector3 particleDirection = rb.velocity.normalized;
        RaycastHit hit;

        if (Physics.Raycast(transform.position, particleDirection, out hit, 1f, ObstacleLayer))
        {
            if (hit.collider.tag == "Immovable")
            {
                float currentParticleSpeed = rb.velocity.magnitude;
                rb.velocity = currentParticleSpeed * Vector3.Reflect(particleDirection, hit.normal);
            }
            else if (hit.collider.tag == "Movable")
            {
                UnityEngine.GameObject movableObject = hit.collider.gameObject;
                if (movableObject.GetComponent<MovableObject>() != null)
                {
                    movableObject.GetComponent<MovableObject>().OnSoundWaveHit(particleDirection);
                }
            }
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if(col.tag == "Particle")
        {
            GameObject objectHit = col.gameObject;
            if(objectHit.GetComponent<ParticlesController>() != null)
            {
                if(objectHit.GetComponent<ParticlesController>().playerOrigin != this.playerOrigin)
                {
                    Collider[] particlesInExplosion = Physics.OverlapSphere(transform.position, 0.0001f, ParticlesLayer);
                    GameManager.instance.RandomInstanciation(transform.position);
                    particlesInExplosion.ToList().ForEach(p => Destroy(p.gameObject));
                    
                }
            }
        }
    }

    public void Launch(Vector3 direction, float force)
    {
        rb.AddForce(direction * force, ForceMode.Impulse);

    }

    IEnumerator DeathTimer()
    {
        yield return new WaitForSeconds(deathTimer);
        Destroy(gameObject);
    }

    IEnumerator ActivateCollider()
    {
        yield return new WaitForSeconds(0.2f);
    }
}
