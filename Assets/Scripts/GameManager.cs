﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public PlayerController player1;
    public PlayerController player2;

    public GameObject[] ObjectsOnExplosion;

    public static GameManager instance { get; private set; }

    void Awake()
    {
        instance = this;
    }

    public void CheckScoreOnFall(int playerIndex)
    {
        if (playerIndex == 1)
            player2.score++;
        else
            player1.score++;
    }

    public void RandomInstanciation(Vector3 explosionPosition)
    {
        int rng = Random.Range(0, ObjectsOnExplosion.Length);
        Instantiate(ObjectsOnExplosion[rng], explosionPosition, Quaternion.identity);
    }
}
