﻿using System.Linq;
using UnityEngine;

public class TestController : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Input.GetJoystickNames().ToList().ForEach(s => print(s + "\n"));
	}
	
	// Update is called once per frame
	void Update () {
        print("Joystick 1 : X value" + Input.GetAxisRaw("Horizontal"));
        print("Joystick 1 : Y value" + Input.GetAxisRaw("Vertical"));

        print("Joystick 2 : X value" + Input.GetAxisRaw("Horizontal2"));
        print("Joystick 2 : Y value" + Input.GetAxisRaw("Vertical2"));
    }
}
