﻿using UnityEngine;
public enum PLAYERSTATE
{
    IDLE,
    SHOUTING,
    SLIDING,
    DEAD
}

public class PlayerController : MonoBehaviour
{
    public int playerNum;
    public PLAYERSTATE playerState;
    public float shoutCooldown;
    public float shoutForce;

    //Score and respawn variables
    private Vector3 SpawnPosition;
    public int score;

    public Sprite[] idleSprites;
    public Sprite[] shoutingSprites;
    public Sprite[] slidingSprites;

    private int direction = 0;
    private int currentDirection = 0;
    public SpriteRenderer rend;

    //Waves spawner
    public GameObject WaveParticle;
    public float WaveRange;

    private float xAxisValue;
    private float yAxisValue;

    private float currentShoutCooldown;

    private Rigidbody rb;

    // Use this for initialization
    void Start()
    {
        score = 0;
        SpawnPosition = transform.position;
        currentShoutCooldown = 0;
        rb = GetComponent<Rigidbody>();
        playerState = PLAYERSTATE.IDLE;
        rend.sprite = idleSprites[0];
    }

    // Update is called once per frame
    void Update()
    {
        xAxisValue = Input.GetAxisRaw("Horizontal" + playerNum);
        yAxisValue = Input.GetAxisRaw("Vertical" + playerNum);

        CheckAngle(xAxisValue, yAxisValue);
        Vector3 direction = new Vector3(xAxisValue, 0, yAxisValue);

        Quaternion lookRotation = Quaternion.LookRotation(direction);
        transform.rotation = lookRotation;

        if (Input.GetButton("ButtonA" + playerNum) && currentShoutCooldown > shoutCooldown)
        {
            rb.velocity = Vector3.zero;
            SpawnSoudWaves(direction.normalized);
            GetComponent<Rigidbody>().AddForce(-direction.normalized * shoutForce, ForceMode.Impulse);
            currentShoutCooldown = 0;
        }
        currentShoutCooldown += Time.deltaTime;
    }

    private void SpawnSoudWaves(Vector3 wavesDirection)
    {
        Vector3 BaseFireDirection = wavesDirection;

        float startingAngle = -WaveRange / 2;

        for (int i = 0; i < WaveRange; i++)
        {
            Vector3 fireDirection = Quaternion.AngleAxis(startingAngle + i, Vector3.up) * BaseFireDirection;
            GameObject waveParticle = Instantiate(WaveParticle, transform.position, Quaternion.identity) as UnityEngine.GameObject;
            waveParticle.GetComponent<ParticlesController>().Launch(fireDirection, 40);
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.tag == "Hole")
        {
            rb.isKinematic = true;
            GameManager.instance.CheckScoreOnFall(playerNum);
            transform.position = SpawnPosition;
            rb.isKinematic = false;
        }
    }
    #region ROTATION_AND_ORIENTATION
    void CheckAngle(float xAngle, float yAngle)
    {
        if (xAngle <= 0.25f && xAngle > -0.25f)
        {
            if (yAngle < 0)
            {
                direction = 0;
            }
            else
            {
                direction = 4;
            }
            
        }
        else if (xAngle <= -0.25f && xAngle > -0.75f)
        {
            if (yAngle < 0)
            {
                direction = 1;
            }
            else
            {
                direction = 3;
            }
        }
        else if (xAngle < -0.75f)
        {
            direction = 2;
        }

        else if (xAngle > 0.75f)
        {
            direction = 6;
        }

        else if(xAngle >= 0.25f && xAngle < 0.75f)
        {
            if (yAngle > 0)
            {
                direction = 5;
            }
            else
            {
                direction = 7;
            }
        }

        Turn();

    }

    void Turn()
    {
        if (currentDirection != direction)
        {
            currentDirection = direction;
            switch(playerState)
            {
                case PLAYERSTATE.IDLE:
                    rend.sprite = idleSprites[direction];
                    break;
                case PLAYERSTATE.SLIDING:
                    rend.sprite = slidingSprites[direction];
                    break;
            }
        }
    }
    #endregion
}

